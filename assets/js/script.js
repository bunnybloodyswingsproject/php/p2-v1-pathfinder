//CSS config

let wall = "rgb(255, 255, 255)";
let original = "rgba(0,0,0, .2)";
let path = "#ffc75f";
//make a maze

let len = 10;
function setup() {
	//maze container
	let maze_container = document.getElementById("maze-container");

	for(let i = 0; i < 10; i++) {
		let	row = document.createElement("div");
		row.className = "row row" + (i + 1);
		row.id = "row" + (i + 1);

		for(let j = 0; j < 10; j++) {
			let node = document.createElement("div");
			node.className = "node node" + ((i * 10)+(j + 1));
			node.id = "node" + ((i * 10)+(j + 1));
			
			if(((i * 10)+(j + 1)) != 1 && ((i * 10)+(j + 1)) != 100) {
				node.style.backgroundColor = original;
				node.onclick = function() {
					clicked(this.id)
				}
			}
			row.appendChild(node);
		}

		maze_container.appendChild(row);
	}


}

function clicked(elementID) {
	let node = document.getElementById(elementID);

	if(node.style.backgroundColor == wall) {
		node.style.backgroundColor = original;
	}else{
		node.style.backgroundColor = wall;
	}
}

//Reset
function reset() {
	for(let i = 2; i < 100; i++) {
		let node = document.getElementById("node" + i);
		node.style.backgroundColor = original;
	}

	document.getElementById("node1").style.backgroundColor = "rgb(255, 38, 38)";
	document.getElementById("node100").style.backgroundColor = "rgb(236, 0, 177)";
}

//Solve the maze
function solveMaze() {
	let maze = [];

	for(let i = 0; i < len; i++) {
		maze[i] = new Array(len).fill(0);
	}

	let rowCount = 0;
	let colCount = 0;
	let nodeVal = 1;

	for(let i = 1;i < (len*len+1); i++) {
		if(document.getElementById("node" + i).style.backgroundColor == wall) {
			maze[rowCount][colCount] = -1;
		}else{
			maze[rowCount][colCount] = nodeVal;
		}

		colCount++;
		if(colCount == len){
			rowCount++;
			colCount = 0;
		}

		nodeVal++;
	}

	// console.log(maze);
	let adjList = [];
	let possibleMoves = [
		[-1,0], //Down --> this is for row
		[1,0], //up --> this is for row
		[0,1], //left --> this is for Column
		[0,-1] //right --> this is for Column
	];

	for(let row = 0; row < maze.length; row++) {
		for(let col = 0; col < maze[row].length; col++) {
			if(maze[row][col] == -1) {
				continue;
			}

			let currNode = maze[row][col];
			let neighbours = [];

			for(let count = 0; count < possibleMoves.length; count++) {
				let nRow = possibleMoves[count][0] + row;
				let nCol = possibleMoves[count][1] + col;

				if((nRow >= 0 && nRow < maze.length) && (nCol >= 0 && nCol < maze[0].length)) {
					if(maze[nRow][nCol] != -1) {
						neighbours.push([nRow, nCol]);
					}
				}
			}
			//in here, together with intergers from 1 to 100,
			//the author add the coordinate in the neighbours variable
			//like [9][6];
			adjList[currNode] = neighbours;
		}
	}

	//solve the maze
	let visited = [];
	let prev = new Array(len*len).fill(0);

	for(let i = 0; i < len; i++) {
		visited[i] = new Array(len).fill(false);
	}

	let queue = [];
	let solved = false;

	queue.push([0,0]);

	 while(queue.length > 0) {
	 	let nodeCoor = queue.splice(0,1)[0];
	 	let node = maze[nodeCoor[0]][nodeCoor[1]];

	 	visited[nodeCoor[0]][nodeCoor[1]] = true;

	 	if(node == 100) {
	 		solved = true;
	 		break;
	 	}

	 	let adj = adjList[node];
	 	for(let count = 0; count < adj.length; count++) {
	 		let n = adj[count];

	 		if(!visited[n[0]][n[1]]) {
	 			visited[n[0]][n[1]] = true;
	 			queue.push(n);
	 			prev[(maze[n[0]][n[1]]) - 1] = node - 1;
	 		}
	 	}
	 }

	 if(!solved) {
	 	alert("This maze is impossible! I'll reset it for you");
	 	reset();
	 	return "";
	 }

	 //retrace
	 let endNode = maze[9][9];

	 document.getElementById("node" + endNode).style.backgroundColor = path;

	 let previous = endNode - 1;
	 let loopControl = false;

	 while(true) {
	 	let node = prev[previous];

	 	try{
	 		document.getElementById("node" + (node + 1)).style.backgroundColor = path;
	 	}catch(err){
	 		loopControl = true;
	 	}

	 	if(node == 0) {
	 		loopControl = true;
	 	}else{
	 		previous = node;
	 	}

	 	if(loopControl) {
	 		break;
	 	}
	 }

	document.getElementById("node1").style.backgroundColor = path;
}

function generate() {
	const min = 1;
	const max = 99;
	let generatedRandInt = 0;
	let randomArr = [];

	generatedRandInt = getRandomArbitrary(min, max);

	randomArr = Array.from({length: generatedRandInt}, () => Math.floor(Math.random() * generatedRandInt));
	
	randomArr = randomArr.filter(function(item, pos) {
		return randomArr.indexOf(item) == pos;
	});

	removeZero(randomArr);
	randomArr.sort();
	console.log(randomArr.sort());
	for(let i = 0; i < randomArr.length; i++) {
		for(j = 0; j < 100 + 1; j++) {
			if(!randomArr[i] == 0) {
				if(randomArr[i] == j) {
					document.getElementById("node" + randomArr[i]).style.backgroundColor = wall;
					document.getElementById("node1").style.backgroundColor = "rgb(255, 38, 38)";
					document.getElementById("node100").style.backgroundColor = "rgb(236, 0, 177)";
				}
			}

		}
	}
}	

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function removeZero(array) {
	const index = array.indexOf(0);

	if(index > -1) {
		array.splice(index, 1);
	}
}