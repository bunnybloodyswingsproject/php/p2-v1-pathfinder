<!DOCTYPE html>
<html>
<head>
	<title>Path Finder JS</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">

	<!-- Font Family-->
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
</head>
<body onload="setup()">

	<div class="this_container">
		<div class="left">
			<div class="maze-container" id="maze-container"></div>
		</div>
		<div class="right">
			<h1>Maze Solver</h1>
			<button class="buttons" onclick="solveMaze()">Solver</button>
			<button class="buttons" onclick="reset()">Reset</button>
			<button class="buttons" onclick="generate()">Generate Walls</button>
		</div>
	</div>




	<script type="text/javascript" src="./assets/js/script.js"></script>
</body>
</html>